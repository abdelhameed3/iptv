/*global $ */
/*eslint-env browser*/
/* Add active  class in home slider */
$(document).ready(function () {
  'use strict';
    
  $(".side-nav .control-side").click(function () {
      $(this).toggleClass("active");
      $('body').toggleClass("side-bar-icon");
  });
  $(".side-nav .control-side").click(function () {
      if( !$("body").hasClass("side-bar-icon" ) ){
          $(".main-panel").addClass("hide-phone");
      }else{
          $(".main-panel").removeClass("hide-phone");
      }
  });
  $('#accordionNav li a img').click(function (){
       if($("body").hasClass("side-bar-icon" )){
           $("body").removeClass("side-bar-icon" );
       }
  });
});
$(document).ready(function () {
  "use strict";
  function reStyle() {
      if ($(window).width() <= 770) {
          $('body').addClass("side-bar-icon");
      } else {
          $('body').removeClass("side-bar-icon");
      }
  }
  reStyle();
  $(window).resize(function () {
      reStyle();
  });
  
});
/*  alarts
============================== */
$(document).ready(function () {
    "use strict";
    /*=========================== Delete ============================== */
    $(".delete").click(function(){
        swal({
            title: "هل أنت متأكد من الحذف ؟",
            text: "بمجرد حذفها ، لن تكون قادراً على استرداد هذا  مرة اخري!",
            icon: "warning",
            buttons: true,
            dangerMode: true,

        })
        .then((willDelete) => {
             if (willDelete) {
                $(this).parent().parent().hide();
                 swal("حسناً! تم الحذف بنجاح", {
                     icon: "success",
                 });
             } else {
                 swal("حسناً! تم إلغاء عملية الحذف");
             }
         });
    });
    /*======================================= btn-toggle ===============================*/
    $(".btn-toggle").click(function (){
        swal({
            title: "هل أنت متأكد من تغير الحالة ؟",
            text: "هل توافق علي تغير الحالة؟ ",
            icon: "warning",
            buttons: true,
            dangerMode: true,

        })
        .then((willDelete) => {
            
             if (willDelete) {
                
                 $(this).toggleClass('active');
                 swal("حسناً! تم التغير بنجاح", {
                     icon: "success",
                 });
             } else {
                 swal("حسناً! تم إلغاء عملية تغير الحالة");
             }
         });
    });
});

/* Add active To aide nav
======================== */
$(document).ready(function () {
    "use strict";
    $("#accordionNav li a").each(function () {
            var t = window.location.href.split(/[?#]/)[0];
            this.href == t && ($(this).addClass("active"), $(this).parent().parent().parent().parent().children('a').addClass("active"));
        })
});

/* video
======================== */
$(document).ready(function () {
    "use strict";
    $('.card-live .controls > div img').click(function (){
        $(this).parent().toggleClass('active');
    });
    $('.card-live .controls > div .playVid').click(function (){
        $(this).parent().parent().parent().parent().find('video').trigger('play');
    });
    $('.card-live .controls > div .pauseVid').click(function (){
        $(this).parent().parent().parent().parent().find('video').trigger('pause');
    });

    $('.card-live .controls  div .muteVid').click(function (){
        $(this).parent().parent().parent().parent().find('video').prop('muted', true);
    });
    $('.card-live .controls  div .unmuteVid').click(function (){
        $(this).parent().parent().parent().parent().find('video').prop('muted', false);
    });
    
});
/* slider
======================== */
$(document).ready(function () {
    "use strict";
    $('.timeStone.slider').slick({
        dots: false,
        nextArrow:".sliderTime .slider-control .arrows .next",
        prevArrow:".sliderTime .slider-control .arrows .prev",
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        rtl:true,
        responsive: [
          {
            breakpoint: 1470,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              
            }
          },
          {
            breakpoint: 1199,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });
});
/* uplaod image
==================================== */

$(function () {

    // Viewing Uploaded Picture On Setup Admin Profile
    function livePreviewPicture(picture)
    {
      if (picture.files && picture.files[0]) {
        var picture_reader = new FileReader();
        picture_reader.onload = function(event) {
          $('#uploaded').attr('src', event.target.result);
        };
        picture_reader.readAsDataURL(picture.files[0]);
      }
    }
  
    $('.uplaod-img input').on('change', function () {
      $('#uploaded').fadeIn();
      livePreviewPicture(this);
    });
  
  });
  
  
  